package com.example.finalproject.network


data class AccessToken (
     val accessToken: String,
     val tokenType: String
)