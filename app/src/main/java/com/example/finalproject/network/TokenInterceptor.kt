package com.example.finalproject.network

import android.content.SharedPreferences
import com.example.finalproject.data.rest.ApiService
import com.example.finalproject.ui.activity.AuthActivity
import okhttp3.Interceptor


class TokenInterceptor(): Interceptor {

//    val sharedPreference:SharedPreferences=SharedPreferences(this)
//    private lateinit var authActivity: AuthActivity

    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        val request = chain.request()
        request.header("Application:json")

//        if(request.header("No-Authentication") == null)
//        }
//        var auth = sharedPreference.
//        request = request.newBuilder().header("Token", token).build()

        return chain.proceed(request)

    }
}