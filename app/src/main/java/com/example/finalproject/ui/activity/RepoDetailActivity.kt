package com.example.finalproject.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.finalproject.R
import com.example.finalproject.data.model.RepoModel
import com.example.finalproject.data.rest.RestApiBuilder
import com.example.finalproject.network.DataSingleton.Companion.REPO_URL
import kotlinx.android.synthetic.main.item_repo.*
import retrofit2.Call
import retrofit2.Response

class RepoDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repo_detail)
        val id = intent.getIntExtra("item_id", 0)
        loadDetails(id)
    }



    private fun loadDetails(id: Int) {

        val service = RestApiBuilder.createService(REPO_URL)
        val requestCall = service.getRepo(id)

        requestCall.enqueue(object : retrofit2.Callback<RepoModel> {

            override fun onResponse(call: Call<RepoModel>, response: Response<RepoModel>) {

                if (response.isSuccessful) {
                    val repo = response.body()
                    repo?.let {
                        name.setText(repo.name)
                        description.setText(repo.description)

                    }
                } else {
                    Toast.makeText(this@RepoDetailActivity, "Failed to retrieve details", Toast.LENGTH_SHORT)
                        .show()
                }
            }

            override fun onFailure(call: Call<RepoModel>, t: Throwable) {
                Toast.makeText(
                    this@RepoDetailActivity,
                    "Failed to retrieve details " + t.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }
}
