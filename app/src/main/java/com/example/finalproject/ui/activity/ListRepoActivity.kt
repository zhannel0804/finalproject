package com.example.finalproject.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.finalproject.R
import com.example.finalproject.data.model.RepoModel
import com.example.finalproject.data.rest.ApiService
import com.example.finalproject.data.rest.RestApiBuilder
import com.example.finalproject.network.DataSingleton.Companion.REPO_URL
import com.example.finalproject.ui.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListRepoActivity : AppCompatActivity(), RepoAdapter.RecyclerViewItemClick {

    lateinit var recyclerView: RecyclerView
    lateinit var progressBar: ProgressBar
    private lateinit var viewModel: RepoViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_repo)

        viewModel = ViewModelProviders.of(this).get(RepoViewModel::class.java)
        recyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)

     //   progressBar = findViewById(R.id.progressBar)

        viewModel.getRepo()
        viewModel.repoLiveData.observe(this, Observer { result ->
            when (result) {
                is RepoResult.RepoList -> {
                    recyclerView.adapter = RepoAdapter(result.list, this)
                }


            }
        })

//        viewModel.progressLiveData.observe(this, Observer {
//            if (it) {
//                progressBar.visibility = View.VISIBLE
//            } else {
//                progressBar.visibility = View.GONE
//            }
//        })

    }
    override fun itemClick(position: Int, item: RepoModel) {

    }
}
