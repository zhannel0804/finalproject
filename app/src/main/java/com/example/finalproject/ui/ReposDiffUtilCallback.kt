package com.example.finalproject.ui

import androidx.recyclerview.widget.DiffUtil
import com.example.finalproject.data.model.RepoModel

class ReposDiffUtilCallback(private val oldList: List<RepoModel>, private val newList: List<RepoModel>) : DiffUtil.Callback() {
    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) = oldList[oldItemPosition].id == newList[newItemPosition].id

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = true
}