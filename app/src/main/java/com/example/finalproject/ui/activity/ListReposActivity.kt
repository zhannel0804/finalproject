package com.example.finalproject.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.finalproject.R
import com.example.finalproject.data.model.RepoModel
import com.example.finalproject.ui.ReposAdapter
import com.example.finalproject.ui.ReposDiffUtilCallback
import com.example.finalproject.ui.ReposResult
import com.example.finalproject.ui.ReposViewModel
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_list_repos.*
import java.util.concurrent.TimeUnit

class ListReposActivity : AppCompatActivity(), ReposAdapter.RecyclerViewItemClick {

    lateinit var recyclerView: RecyclerView
    lateinit var progressBar: ProgressBar
    private lateinit var viewModel: ReposViewModel
    private val disposable = CompositeDisposable()
    //private val reposAdapter = recyclerView.adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_repos)

        viewModel = ViewModelProviders.of(this).get(ReposViewModel::class.java)
        recyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)



        progressBar = findViewById(R.id.progressBar)

        viewModel.getRepos()
        viewModel.reposLiveData.observe(this, Observer { result ->
            when (result) {
                is ReposResult.ReposList -> {
                    recyclerView.adapter = ReposAdapter(result.list, this)
                }


            }
        })

        viewModel.progressLiveData.observe(this, Observer {
            if (it) {
                progressBar.visibility = View.VISIBLE
            } else {
                progressBar.visibility = View.GONE
            }
        })

        searchInput
            .textChanges()
            .debounce(200, TimeUnit.MILLISECONDS)
            .subscribe {
                viewModel
                    .search(this.toString())
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        val diffResult = DiffUtil.calculateDiff(ReposDiffUtilCallback(viewModel.oldFilteredRepos, viewModel.filteredRepos))
                        viewModel.oldFilteredRepos.clear()
                        viewModel.oldFilteredRepos.addAll(viewModel.filteredRepos)
                        //diffResult.dispatchUpdatesTo(reposAdapter)
                    }.addTo(disposable)
            }.addTo(disposable)


    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }
    override fun itemClick(position: Int, item: RepoModel) {

    }
}