package com.example.finalproject.ui

import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.finalproject.data.rest.RestApiBuilder
import com.example.finalproject.network.AccessToken
import com.example.finalproject.network.DataSingleton.Companion.AUTH_URL
import com.example.finalproject.network.DataSingleton.Companion.CLIENT_ID
import com.example.finalproject.network.DataSingleton.Companion.CLIENT_SECRET
import com.example.finalproject.ui.activity.AuthActivity
import com.example.finalproject.ui.activity.ListRepoActivity
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AuthViewModel : ViewModel(){
    lateinit var authActivity: AuthActivity

    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

//    fun getToken() {
//        val api = RestApiBuilder.createService(AUTH_URL)
//        api.getAccessToken(code, CLIENT_ID, CLIENT_SECRET).enqueue(object : Callback<AccessToken> {
//            override fun onFailure(call: Call<AccessToken>, t: Throwable) {
//                Toast.makeText(authActivity, "Error", Toast.LENGTH_SHORT).show()
//            }
//
//            override fun onResponse(call: Call<AccessToken>, response: Response<AccessToken>) {
//                if(response.isSuccessful) {
//                    val token = response.body()
//                    Log.e("TAG", "access = $token")
//
//                }
//                else {
//                    Toast.makeText(authActivity, "Error", Toast.LENGTH_SHORT).show()
//                }
//            }
//        })
//    }



    override fun onCleared() {
        super.onCleared()
    }


}