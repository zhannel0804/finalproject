package com.example.finalproject.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.finalproject.R
import com.example.finalproject.data.model.RepoModel

class RepoAdapter(
    var list: List<RepoModel>? = null,
    val itemClickListener: RecyclerViewItemClick? = null) : RecyclerView.Adapter<RepoAdapter.PostViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PostViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_repo, p0, false)
        return PostViewHolder(view)
    }

    override fun getItemCount(): Int = list?.size ?: 0

    override fun onBindViewHolder(p0: PostViewHolder, p1: Int) {
        p0.bind(list?.get(p1))
    }

    inner class PostViewHolder(val view: View): RecyclerView.ViewHolder(view) {

        fun bind(repo: RepoModel?) {
            val tvName = view.findViewById<TextView>(R.id.name)
            val tvDescription = view.findViewById<TextView>(R.id.description)

            tvName.text = repo?.name
            tvDescription.text = repo?.description

            view.setOnClickListener {
                itemClickListener?.itemClick(adapterPosition, repo!!)
            }
        }
    }

    interface RecyclerViewItemClick {

        fun itemClick(position: Int, item: RepoModel)
    }
}