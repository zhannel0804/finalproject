package com.example.finalproject.ui

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.finalproject.data.model.RepoModel
import com.example.finalproject.data.rest.RestApiBuilder
import com.example.finalproject.network.DataSingleton.Companion.REPO_URL
import kotlinx.coroutines.*

class RepoViewModel : AndroidViewModel {

    constructor(application: Application) : super(application)

    val repoLiveData = MutableLiveData<RepoResult>()
    val progressLiveData = MutableLiveData<Boolean>()
    private val myJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + myJob)

    fun getRepo() {
        progressLiveData.value = true
        uiScope.launch {
            val list = withContext(Dispatchers.IO) {
                RestApiBuilder.createService(REPO_URL).getRepos()
            }
            repoLiveData.value = RepoResult.RepoList(list)
            progressLiveData.value = false

        }
    }
}
sealed class RepoResult {
    data class RepoList(val list: List<RepoModel>?) : RepoResult()
}