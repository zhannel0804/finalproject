package com.example.finalproject.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.finalproject.R
import com.example.finalproject.data.model.RepoModel

class ReposAdapter(
    var list: List<RepoModel>,
    val itemClickListener: RecyclerViewItemClick) : RecyclerView.Adapter<ReposAdapter.PostViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PostViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_repos, p0, false)
        return PostViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(p0: PostViewHolder, p1: Int) {
        p0.bind(list.get(p1))
    }

    inner class PostViewHolder(val view: View): RecyclerView.ViewHolder(view) {

        fun bind(repos: RepoModel) {
            val tvName = view.findViewById<TextView>(R.id.name)
            val tvDescription = view.findViewById<TextView>(R.id.description)

            tvName.text = repos.name
            tvDescription.text = repos.description

            view.setOnClickListener {
                itemClickListener.itemClick(adapterPosition, repos)
            }
        }
    }

    interface RecyclerViewItemClick {

        fun itemClick(position: Int, item: RepoModel)
    }
}