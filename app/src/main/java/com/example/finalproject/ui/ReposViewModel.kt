package com.example.finalproject.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.finalproject.data.model.RepoModel
import com.example.finalproject.data.rest.RestApiBuilder
import com.example.finalproject.network.DataSingleton.Companion.REPO_URL
import io.reactivex.Completable
import kotlinx.coroutines.*




class ReposViewModel  : AndroidViewModel {

    constructor(application: Application) : super(application)


    private val originalRepos = listOf<RepoModel>()
    val filteredRepos: MutableList<RepoModel> = mutableListOf()
    val oldFilteredRepos: MutableList<RepoModel> = mutableListOf()

    val reposLiveData = MutableLiveData<ReposResult>()
    val progressLiveData = MutableLiveData<Boolean>()
    private val myJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + myJob)

    init {
        oldFilteredRepos.addAll(originalRepos)
    }

    fun search(query: String): Completable = Completable.create {
        val wanted = originalRepos.filter {
            it.name.contains(query) || it.description.contains(query)
        }.toList()

        filteredRepos.clear()
        filteredRepos.addAll(wanted)
        it.onComplete()
    }

    fun getRepos() {
        progressLiveData.value = true
        uiScope.launch {
            val list = withContext(Dispatchers.IO) {
               RestApiBuilder.createService(REPO_URL).getReposCoroutine()
            }
            reposLiveData.value = ReposResult.ReposList(list)
            progressLiveData.value = false

        }
    }
}
sealed class ReposResult {
    data class ReposList(val list: List<RepoModel>) : ReposResult()
}