package com.example.finalproject.ui.activity


import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_auth.*
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.finalproject.R
import com.example.finalproject.data.rest.RestApiBuilder
import com.example.finalproject.data.rest.RestApiBuilder.Companion.createService
import com.example.finalproject.network.AccessToken
import com.example.finalproject.network.DataSingleton
import com.example.finalproject.network.DataSingleton.Companion.AUTH_URL
import com.example.finalproject.network.DataSingleton.Companion.AUTH_URL_BASE
import com.example.finalproject.network.DataSingleton.Companion.CLIENT_ID
import com.example.finalproject.network.DataSingleton.Companion.CLIENT_SECRET
import com.example.finalproject.network.DataSingleton.Companion.REDIRECT_URI
import com.example.finalproject.ui.AuthViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T





class AuthActivity : AppCompatActivity() {
    private lateinit var model: AuthViewModel
    private lateinit var preferences: SharedPreferences
    private var shared_token = "newToken"
    private var shared_pref = "MySharedPref"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        val myClass = AuthActivity(this)

        model = ViewModelProviders.of(this)[AuthViewModel::class.java]

        sign_in.setOnClickListener {
            val tabsIntent = CustomTabsIntent.Builder().build()
            val url = "$AUTH_URL_BASE?client_id=$CLIENT_ID&redirect_uri=$REDIRECT_URI&scope=gist&allow_signup=false"
            tabsIntent.launchUrl(this, Uri.parse(url))
        }
        pass.setOnClickListener {
            val intent1 = Intent(this, ListReposActivity::class.java)
            startActivity(intent1)
            finish()
        }

        preferences = getSharedPreferences("MySharedPref", Context.MODE_PRIVATE)
    }

    class AuthActivity(context: Context) {

        val context: Context = context

    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        val data = intent?.data
        if (data != null && data.toString().startsWith(REDIRECT_URI)) {
            val code = data.getQueryParameter("code")
            if (code != null) {
                val api = createService(AUTH_URL)
                api.getAccessToken(code, CLIENT_ID, CLIENT_SECRET).enqueue(object : Callback<AccessToken> {
                    override fun onFailure(call: Call<AccessToken>, t: Throwable) {
                        Toast.makeText(this@AuthActivity, "Error", Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<AccessToken>, response: Response<AccessToken>) {
                        if (response.isSuccessful) {
                            val newToken = response.body().toString()
                            if(preferences.contains(shared_token)) {
                                val editor = preferences.edit()
                                editor.putString(shared_token, newToken)
                                editor.apply()
                            }
                            val intent1 = Intent(this@AuthActivity, ListRepoActivity::class.java)
                            startActivity(intent1)
                            finish()
                            Log.e("TAG", "access = $newToken")
                        } else {
                            Toast.makeText(this@AuthActivity, "Error", Toast.LENGTH_SHORT).show()
                        }
                    }

                })
            } else {
                Toast.makeText(this@AuthActivity, "Error", Toast.LENGTH_SHORT).show()
            }
        }
    }
}






