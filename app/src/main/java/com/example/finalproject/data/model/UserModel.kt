package com.example.finalproject.data.model

import androidx.room.Entity

@Entity
data class UserModel(
    val login: String,
    val id: Long,
    val followers_url: String,
    val following_url: String
)