package com.example.finalproject.data.model

import androidx.room.Entity

@Entity
data class RepoModel (
    var id: Int,
    var name: String,
    var description: String,
    var stargazers_count: Float)