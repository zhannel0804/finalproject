package com.example.finalproject.data.rest

import com.example.finalproject.data.model.RepoModel
import com.example.finalproject.network.AccessToken
import com.example.finalproject.network.TokenInterceptor
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiService {
    @Headers("Accept:application/json")
    @FormUrlEncoded
    @POST("login/oauth/access_token")
    fun getAccessToken(
        @Field("code") code: String,
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String) : Call<AccessToken>

    @GET("/user/repos")
    suspend fun getRepos() : List<RepoModel>


    @GET("/repositories")
    suspend fun getReposCoroutine(): List<RepoModel>

    @GET("/repositories")
    fun getRepo(@Path ("id") id: Int): Call<RepoModel>
}

//@Query("sort") sort: String,
//@Query("order") order: String,
//@Query("page") page: Long